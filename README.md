Little Games
============

Ansammlung von guten Spielen

Dabei sind Spiele welche schnell in kleinen bis großen Gruppen, drinnen oder draußen gespielt werden können.

Alle Spiele stehen unter CC0 und können von allen Menschen ohne weiteres verwendet werden.

Falls ihr weitere Spiele habt welche ihr hier nicht findet, schreibt Pull Requests.

Zusätzliche Erweiterungen sind im Aufbau.

Die Syntax für weitere Spiele ist:
```
# Spieltitle

## Benötigt
* 1 Apfel
* 2 Bananen

(## Kurzbeschreibung)
(Es geht darum Spaß zu haben)

## Beschreibung
Das Ziel dieses so großen Spiels, ist es möglichst mit allen Spielenden Spaß zu bereiten.

(## Varianten)
(* ohne Spaß :D)

(## siehe auch)
([Entwurf in English](./entwurf-en.md))

## Tags
* entwurf
* README
```
