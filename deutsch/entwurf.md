# Spieltitel

## Benötigt
* 1 Apfel
* 2 Bananen

## Kurzbeschreibung
Es geht darum Spaß zu haben

## Beschreibung
Das Ziel dieses so großen Spiels, ist es möglichst mit allen Spielenden Spaß zu bereiten.

## Varianten
* ohne Spaß :D

## siehe auch
[Entwurf in English](./entwurf-en.md)

## Tags
* entwurf
* README
