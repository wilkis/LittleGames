# Jugendarbeitethik

* Der Zugang zu Jugendarbeit und allem, was einem zeigen kann, wie diese Welt funktioniert, sollte unbegrenzt und vollständig sein.
* Alle Informationen müssen frei sein. Trink genügend Wasser.
* Übersehe nicht andere Probleme – sei hilfsbereit, teamfähig und ein gutes Vorbild.
* Beurteile eine Betreuerin nach dem, was sie tut, und nicht nach üblichen Kriterien wie Aussehen, Alter, Herkunft, Spezies, Geschlecht oder gesellschaftliche Stellung.
* Man kann mit Jugendarbeit Spaß und Freude schaffen.
* Zeltlager können dein Leben zum Besseren verändern.
* Mülle nicht und räume auch den Müll von anderen Leuten weg.
* Öffentliche Ohren haben, private Peinlichkeiten schützen.

Die Jugendarbeitethik ist nicht einheitlich definiert. Die ursprüngliche Version stammt aus der [Hackerethik des Chaos Computer Club](https://www.ccc.de/hackerethik).

Verbesserungsvorschläge und Eingaben dazu gerne jederzeit als [Issue](https://codeberg.org/wilkis/LittleGames/issues) erstellt werden.
