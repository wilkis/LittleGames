# Apfel aus Wassereinmer holen

## Benötigt
* Eimer mit Wasser
* mehrere Äpfel

## Beschreibung
Im Eimer ist ein Apfel, dieser wird ohne Hände heraus gefischt. Es gewinnt die Gruppe oder der Spielende mit der kürzsten Zeit.

## Tags
* Lebensmittel
* Zeit
