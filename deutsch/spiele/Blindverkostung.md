# Blindverkostung

## Benötigt
* verschiedene Lebensmittel

## Beschreibung
Die verschiedene Lebensmittel werden bind von den Spielenden verkostet und diese müssen erraten, um welche Lebensmittel es sich handelt.

### Hinweis
Achte auf Allergien und Lebensmittelunverträglichkeiten

## Varianten
* mit Tee
* 18+ Edition: mit Alkohol

## Tags
* Lebensmittel
