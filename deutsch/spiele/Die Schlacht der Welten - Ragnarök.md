Die Schlacht der Welten/ Ragnarök
==================================

## Benötigt
* Schwämme (1x p.P.)
* Schwimmnudel (x3, jeweils verschiedene Farben)
* 3x z.B. Heuballen für ein "Lager"
* 3x Flaggen
* Gesichtsfarbe, Geschichte, Requisiten
* Versorgungswagen (Tische, Trinken, Becher, Erste Hilfe)
* gemähte Wiese

## Beschreibung
Es bestehen drei Teams: Zwerge, Riesen und Elfen.
Pro Team gibt es eine Flagge, die jeweils von den anderen Teams geklaut und zum anderen Stützpunkt gebraucht werden muss. 
Jeder Spieler hat einen Schwamm, mit dem es die anderen Teams bewerfen kann. 
Die Getroffenen kehren nach "Walhalla" (Versorgungswagen) und werden versorgt. Danach kehren die Spieler zurück zu ihrem Stützpunkt und starten von neu. 
Die Schwämme auf dem Feld können frei von jeden genutzt werden, nur die Schwimmnudel gilt für je ein Team. Denn jedes Team besitzt eine eigene Farbe und einen Träger, der mit dieser die Schwämme abwehren und die gegnerischen Spieler treffen kann.
Wird der Schwimmnudelträger getroffen, lässt er die Schwimmnudel dort liegen, wo er gerade ist. Sie darf von jeder Spieler aus dem eigenen Team genommen werden. 
Das Gewinnerteam muss alle Flaggen besitzen.

## Hinweis

## Varianten

## Tags
* draußen
