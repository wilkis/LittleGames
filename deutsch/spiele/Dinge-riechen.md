# Dinge riechen

## Benötigt
* *10* Joghurtbecher
* Alufolie
* *10* geruchsstarke verschiedene Dinge

## Beschreibung
Die Joghurtbecher werden mit *10* verschiedenen geruchsstarken Dingen befüllt, in Alufolie verpackt und oben ein Loch rein gestochen. Nun gilt es zu erraten was sich in den Joghurtbechern gefindet. Es gewinnt die Gruppe, mit den meist richtig eratenden Dingen.

## Tags
* Lebensmittel
