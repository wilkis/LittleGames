# Dosenwerfen

## Benötigt
* X Dosen
* Ball

## Beschreibung
Die Dosen werden auf aufeinander gestapelt und werden dann mit einem Ball versucht abzuwerfen. Es gewinnen die Spielenden, welche am meisten oder alle Dosen abwerfen.

## Tags
* Wurfspiel
