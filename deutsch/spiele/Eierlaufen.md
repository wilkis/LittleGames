# Eierlaufen

## Benötigt
* Löffel
* Eier

## Beschreibung
Der Löffel mit dem Ei drin in einer Hand, muss möglichst schnell von A an B transportiert werden. Es gewinnt die Gruppe, welche am schnellsten oder am meisten Runden in einer bestimmten Zeit laufen.

## Tags
* Lebensmittel
* Zeit
