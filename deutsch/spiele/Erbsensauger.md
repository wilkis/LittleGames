# Erbsensauger

## Benötigt
* (*Anzahl der Spielende*+x) Strohhalm
* (*Anzahl der Spielende*+1) Glas oder Becher
* Erbsen
* Stoppuhr

## Beschreibung
Ziel ist es so viele Erbsen wie möglich vom ersten Glas über alle Glässer hinweg bis zu letzte Glas zu transportieren. Ausschlaggebend ist die Anzahl der Erbsen im letzen Glas. Die Erbsen werden vom ersten Spielenden angesaugt und **ohne Hände** in das Glas des genachbarten Spielenden befördert.

## Siehe auch
* [Strohhalmspiel](./Strohhalmspiel.md)

## Tags
* Zeit
* Lebensmittel
