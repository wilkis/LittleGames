# Gummistiefelweitwurf

## Benötigt
* Gummistiefel
* Messgerät z.B. ein Zollstock

## Beschreibung
Die Spielenden werfen einen Gummistiefel und wer diesen am weitesten wirft, gewinnt

## Tags
* draußen
* Wurfspiel
