# Improtheater

## Benötigt
* keine

## Beschreibung
Die Spielende improvisieren auf der Bühne eine Show. Wie die Vorführung ihren Weg läuft, ist den Spielenden überlassen. Sobald eine Person aus dem Publikum klascht, tauscht diese einen Spielenden gegen sich selber aus.

## Varianten
* die klaschende Person darf einen Handlungsstrang vorgeben

## Tags
* Schauspiel
