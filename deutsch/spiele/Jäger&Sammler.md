# Jäger & Sammler

## Benötigt
* freie Fläche
* Strohhalme
* Fingerfarbe
* Tücher
* günstige Bälle
* Flatterband

## Kurzbeschreibung
Die Gruppen müssen verschiedene Gegenstände sammeln und sich von den jagenden Betreuern fernhalten.

## Beschreibung
Die Spielenden dürfen sich in einem abgestecktem Feld, in gegenüberliegende Ecken eine Basis für ihr Gruppe bauen.

Die Gegenstände(Tücher, Strohhalme, Bälle) liegen leicht versteckt im abgestecktem Feld.

Die Spielenden müssen die Gegenstände sammeln und in die Basis ihres Gruppe bringen. Es dürfen pro Person nur eine Gegenstand in die Basis gebracht werden.

Dabei können sie von den Spielenden der anderen Gruppe gefangen werden und müssen sich wenn sie getickt wurden, hinsetzen.

Die Spielenden können sich durch das abklatschen ihrer Mitspielenden wieder befreien.

Das Spiel geht über einem bestimmten Zeitraum (z.B. 20 Min.)

Am Ende werden die Punkte gezählt. Die Gruppe mit dem meisten Punkten gewinnt.

Eine Gruppe kann vorzeitig verlieren, wenn alle Spielende von den Spielenden einer anderen Gruppe getickt werden.

### Punkteverteilung
zum Beispiel:

Gegenstände | Punkte
----------- | ------
Strohhalm | 3 Punkte
Ball | 1 Punkt
Tücher | 2 Punkte

## Tags
* draußen
* Zeit
