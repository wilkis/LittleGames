# Kartenlauf

## Benötigt
* Kartenspiel

## Beschreibung
An einem Ende des Spielfelds liegen verdeckt und in zufälliger Reihenfolge die Karten eines Spielsets. Am anderen Ende des Spielfeld stehen die Spielenden, die sich einer Gruppe nacheinander in eine Reihe stellen. Ziel ist es alle Karten einer Farbe **rückwärts**(A, K, Q, B, 10, 9, 8, 7) zu sammeln. Nun muss **jeweils ein** Spielender aus der Gruppe zum anderen Ende des Spielfelds laufen. Dort muss eine Karte aufdeckt werden, sollte dies nicht die gewünschte Karte sein wird die Karte wieder verdeckt und zurück gelaufen. Das erste Ass was ein Spielender für seine Gruppe findet, wird die Farbe welche die Gruppe sammelt.

## Tags
* Karten
* Laufspiel
* Zeit
