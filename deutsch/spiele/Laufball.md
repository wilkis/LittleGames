# Laufball

## Benötigt
* 1 Ball
* 4 Makierungen
* 1 Stoppuhr

## Beschreibung
Das Spiel läuft ab in zwei Runden. Es gibt ein Lauf- und ein Ballteam, welches nach einer Runde wechselt. Während eine Person aus dem Laufteam einmal um das makierte Feld läuft, wird der Ball beim Ballteam einmal im Kreis gereicht/geworfen. Es gewinnt das Laufteam was am wengisten Balldurchläufe gebraucht hat.

Beginnet mit dem Einwurf des Ball über die Grundlinie, läuft der erste Spielende des Laufteam los. Sobald der Ball des Ballteam einmal im Kreis gewandert ist, wird dieser über die Grundlinie geworfen und die Laufende des Laufteam mussen stoppen. Erst wenn der Ball erst wieder über die Grundlinie bei Ballteam geworfen wird, darf der Laufende des Laufteams wieder loslaufen. Es darf immer nur ein Spielender des Laufteams laufen. Sobald ein Laufender des Laufteams seine Runde beendet hat und der Ball im Ballteam noch rund geht, darf ein Laufender des Laufteam loslaufen.

## Tags
* Zeit
* draußen
* Laufspiel
* Wurfspiel
