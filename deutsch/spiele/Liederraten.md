# Lieder raten

## Benötigt
* Musikbox

## Beschreibung
Es werden Lieder von einer Person abgespielt und die anderen Spielenden müssen die Lieder erraten.

## Varianten
* Liedermacher.Innen können in der Reihe rum gehen.

## Tags
* Musik
