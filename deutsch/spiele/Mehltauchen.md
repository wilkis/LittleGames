# Mehltauchen

## Benötigt
* Wasser
* 2 Töpfe
* Gummibärchen
* Mehl

## Beschreibung
Gummibärchen aus einem Wassertopf holen und danach Gummibärchen aus dem Mehltopf holen. Die Zeit wird gestoppt. Es gewinnen die Spielende, welche am wenigsten Zeit brauchten und am besten aussehen.

### Hinweis
Achte auf Allergien und Lebensmittelunverträglichkeiten

## Tags
* Lebensmittel
* Zeit
