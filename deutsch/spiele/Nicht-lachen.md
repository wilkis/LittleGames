# Nicht lachen

## Benötigt
* Wasser

## Beschreibung
Eine Person ist auf der Bühne und versucht Witze zu erzählen oder Witz zu sein. Das Publikum bzw. die Mitspielenden haben Wasser im Mund und dürfen nicht lachen. Wer anfängt zu lachen, wird sein Wasser ausspucken und verliert.

# Varianten
* ohne Wasser

## Tags
* Witzig
