# Schieb- / Schubkarrenrennen

## Benötigt
* Pfeiler (Makierungspunkt)

## Beschreibung
Ein Spielender muss sich auf dem Bauch legen und sich mit den Händen abstützen. Der zweite Spielende nimmt die Unterbeine des ersten Spielenden, somit ist die Schiebkarre fertig. Nun müssen die beiden Spielenden einmal um den Pfeiler und wieder zurück zum Start. Die Gruppe welche am schnellsten alle ihre Spielenden durch das Spiel gebracht hat, gewinnt!

## Tags
* simple
* Zeit
