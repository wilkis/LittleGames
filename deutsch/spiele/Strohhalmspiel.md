# Strohhalmspiel

## Benötigt
* (*Anzahl der Spielende*+x) Strohhalm
* (*Anzahl der Spielende*+1) Glas oder Becher
* 1 Tischtennisball
* Stoppuhr

## Beschreibung
Ziel ist es den Tischtennisball vom ersten Glas über alle Glässer hinweg bis zu letzte Glas zu bekommen. Ausschlaggebend ist die Zeit. Der Tischtennisball wird vom ersten Spielenden angesaugt und **ohne Hände** in das Glas des genachbarten Spielenden transportiert.

## Siehe auch
* [Erbsensauger](./Erbsensauger.md)

## Tags
* Zeit
* Erbsensauger
