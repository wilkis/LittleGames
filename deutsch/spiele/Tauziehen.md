# Tauziehen

## Benötigt
* Tau (Seil)

# Beschreibung
An beiden Enden des Tau stehen alle Gruppenmitglieder und halten das Tau. Ziel ist es die Mitte des Taus über eine bestimmte Linie zu bekommen. Auf **LOS!** ziehen die Gruppenmitglieder an ihrem Tau und versuchen ihr Ziel zu erreichen. Hierbei spielen die Gruppenstärke sowie die Taktik wichtige Rolle.

## Tags
* simple
* draußen
