# Teebeutelweitwurf

## Benötigt
* Teebeutel
* Messgerät z.B. ein Zollstock

## Beschreibung
Eine jeweilige Person nimmt einen Teebeutel in den Mund und wirft ihn so weit wie es geht.
Wer den Beutel am weitesten wirft gewinnt.

## Tags
* Lebensmittel
* Wurfspiel
