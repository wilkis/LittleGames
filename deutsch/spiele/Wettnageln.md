# Wettnageln

## Benötigt
* Nägel
* 2+ Hammer
* Holzbalken oder Holzbrett

## Beschreibung
Alle Spielenden haben einen Hammer und ein Nagel. Wer als erste.r den Nagel vollständig im Holzbrett versenkt habt, gewinnt.

## Varianten
* wenn nur wenige Hammer zur Verfügung stehen, kann dies Zeit mit einer Stoppuhr gemessen werden

## Tags
* Holz
* draußen
