# Zehnerball

## Benötigt
* 1 Fußball

## Beschreibung
Um eine Runde zu gewinnen muss eine Gruppe den Ball *10*-mal hin und her gespielt haben. Dabei darf die andere Gruppe den Ball klauen allerdings dürfen die generischen Spielenden nicht berührt werden. Der Ball muss innerhalb von *5* Sekunden weitergeschlossen werden, sonst fällt der Ball an die generische Gruppe.

## Tags
* Fußball
* draußen
