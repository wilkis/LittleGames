# auf den Tisch des Hauses

## Benötigt
* alles

## Beschreibung
Die Spielmoderation fragt die Gruppen nach verschiedene Gegenstände wie Klopapier, Zahnpasta oder einer bestimmten Person. Die Gruppe welche den Gegenstand als erstes zur Spielmoderation bringt, gewinnt.

## Tags
* Zeit
